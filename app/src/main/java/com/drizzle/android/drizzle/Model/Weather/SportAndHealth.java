package com.drizzle.android.drizzle.model.Weather;

import com.drizzle.android.drizzle.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by macbookpro on 02.11.15.
 */
public class SportAndHealth {
    Double mPrecipProbNow;
    Double mPressure;
    Double mHumidity;
    Double mApparentTemp;

    protected static SportAndHealth setSportAndHealthData(JSONObject data) throws JSONException {
        SportAndHealth sportAndHealth = new SportAndHealth();
        JSONObject jsonCurrently = data.getJSONObject("currently");

        sportAndHealth.setPrecipProbNow(jsonCurrently.getDouble("precipProbability"));
        sportAndHealth.setHumidity(jsonCurrently.getDouble("humidity"));
        sportAndHealth.setPressure(jsonCurrently.getDouble("pressure"));
        sportAndHealth.setApparentTemp(jsonCurrently.getDouble("apparentTemperature"));

        return sportAndHealth;
    }

    public Double getPrecipProbNow() {
        return mPrecipProbNow;
    }

    public void setPrecipProbNow(Double precipProbNow) {
        mPrecipProbNow = precipProbNow;
    }

    public Double getPressure() {
        return mPressure;
    }

    public void setPressure(Double pressure) {
        mPressure = pressure;
    }

    public Double getHumidity() {
        return mHumidity;
    }

    public void setHumidity(Double humidity) {
        mHumidity = humidity;
    }

    public Double getApparentTemp() {
        return mApparentTemp;
    }

    public void setApparentTemp(Double apparentTemp) {
        mApparentTemp = apparentTemp;
    }

    public int getClothesIconId() {
        if(mApparentTemp < 14.0) {
            return R.drawable.jacket;
        } else if(mApparentTemp >= 14.0 && mApparentTemp <= 19.0) {
            return R.drawable.jumper;
        } else return R.drawable.tshirt;
    }

    public int getPressureIconId() {
        if(mPressure < 1000.0) {
            return R.drawable.dizzy;
        } else return R.drawable.ok;
    }
}
