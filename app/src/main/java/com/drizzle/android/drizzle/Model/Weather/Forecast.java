package com.drizzle.android.drizzle.model.Weather;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by macbookpro on 28.10.15.
 */
public class Forecast {
    private Currently mCurrently;
    private SportAndHealth mSportAndHealth;
    private String mAddress;

    public static Forecast setForecast(JSONObject response) throws JSONException {
        Forecast mForecast = new Forecast();

        mForecast.setCurrently(Currently.setCurrentlyData(response));
        mForecast.setSportAndHealth(SportAndHealth.setSportAndHealthData(response));

        return mForecast;
    }

    public Currently getCurrently() {
        return mCurrently;
    }

    public void setCurrently(Currently currently) {
        mCurrently = currently;
    }

    public SportAndHealth getSportAndHealth() {
        return mSportAndHealth;
    }

    public void setSportAndHealth(SportAndHealth sportAndHealth) {
        mSportAndHealth = sportAndHealth;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }
}
