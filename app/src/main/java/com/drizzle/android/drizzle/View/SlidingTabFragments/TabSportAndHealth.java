package com.drizzle.android.drizzle.view.SlidingTabFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.drizzle.android.drizzle.model.Weather.Forecast;
import com.drizzle.android.drizzle.model.Weather.SportAndHealth;
import com.drizzle.android.drizzle.R;

/**
 * Created by macbookpro on 02.11.15.
 */
public class TabSportAndHealth extends Fragment {

    private int mClothesIconIdToSet;
    private int mPressureIconIdToSet;
    private Double mPressure;
    private Double mPrecipProb;
    private Double mHumidity;
    private Double mTemp;

    private ImageView mClothesIcon;
    private ImageView mPressureIcon;
    private TextView mClothesText;
    private TextView mPressureText;
    private TextView mUmbrellaText;
    private TextView mHumidityText;

    public static TabSportAndHealth newInstance(Forecast forecast) {
        TabSportAndHealth tabSportAndHealth = new TabSportAndHealth();
        SportAndHealth sportAndHealth = forecast.getSportAndHealth();
        Bundle args = new Bundle();

        args.putInt("CLOTHES_ICON", sportAndHealth.getClothesIconId());
        args.putInt("PRESSURE_ICON", sportAndHealth.getPressureIconId());
        args.putDouble("TEMP", sportAndHealth.getApparentTemp());
        args.putDouble("PRESSURE", sportAndHealth.getPressure());
        args.putDouble("PRECIP_PROB", sportAndHealth.getPrecipProbNow());
        args.putDouble("HUMIDITY", sportAndHealth.getHumidity());
        tabSportAndHealth.setArguments(args);
        return tabSportAndHealth;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();

        mClothesIconIdToSet = args.getInt("CLOTHES_ICON");
        mPressureIconIdToSet = args.getInt("PRESSURE_ICON");
        mPrecipProb = args.getDouble("PRECIP_PROB");
        mPressure = args.getDouble("PRESSURE");
        mHumidity = args.getDouble("HUMIDITY");
        mTemp = args.getDouble("TEMP");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sportandhealth, container, false);

        setViews(v);

        return v;
    }

    private void setViews(View v) {
        mClothesIcon = (ImageView) v.findViewById(R.id.clothesIcon);
        mPressureIcon = (ImageView) v.findViewById(R.id.pressureIcon);
        mClothesText = (TextView) v.findViewById(R.id.clothesText);
        mPressureText = (TextView) v.findViewById(R.id.pressureText);
        mUmbrellaText = (TextView) v.findViewById(R.id.rainText);
        mHumidityText = (TextView) v.findViewById(R.id.humidityText);

        mClothesIcon.setImageResource(mClothesIconIdToSet);
        mPressureIcon.setImageResource(mPressureIconIdToSet);

        if(mTemp < 14.0) {
            mClothesText.setText(R.string.cold);
        } else if(mTemp < 19.0) {
            mClothesText.setText(R.string.middling);
        } else mClothesText.setText(R.string.warm);

        if(mPressure < 1000.0) {
            mPressureText.setText(R.string.low_pressure);
        } else mPressureText.setText(R.string.normal_pressure);

        if(mPrecipProb > 0.7) {
            mUmbrellaText.setText(R.string.rain);
        } else if(mPrecipProb > 0.3) {
            mUmbrellaText.setText(R.string.rain_maybe);
        } else mUmbrellaText.setText(R.string.rain_no);

        if(mHumidity > 0.35 && mHumidity < 0.6) {
            mHumidityText.setText(R.string.humidity_ok);
        } else if(mHumidity <= 0.35) {
            mHumidityText.setText(R.string.humidity_low);
        } else mHumidityText.setText(R.string.humidity_high);
    }
}
