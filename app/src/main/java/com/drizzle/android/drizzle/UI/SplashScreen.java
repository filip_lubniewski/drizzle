package com.drizzle.android.drizzle.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.drizzle.android.drizzle.R;

public class SplashScreen extends AppCompatActivity {

    private boolean mIsDialogShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        TextView retryButton = (TextView) findViewById(R.id.retry_button);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;

                if(isOnline() == false) {
                    intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                } else if(isLocationEnabled() == false) {
                    intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                } else if(isOnline() && isLocationEnabled())
                if(isOnline() && isLocationEnabled()) {
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            }
        };

        retryButton.setOnClickListener(listener);

        startMainIfPossible();
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void startMainIfPossible() {
        if(isOnline() == false || isLocationEnabled() == false) {
            if (mIsDialogShown == false) {
                mIsDialogShown = true;
                new AlertDialog.Builder(this)
                        .setTitle("Be careful!")
                        .setMessage("You probably have no internet connection or you have disabled location services.\n" +
                                "Turn them on manually or click our Retry button!:)")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mIsDialogShown = false;
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        }
        else {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startMainIfPossible();
    }
}
