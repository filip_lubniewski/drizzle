package com.drizzle.android.drizzle.view.SlidingTabFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.drizzle.android.drizzle.model.Weather.Currently;
import com.drizzle.android.drizzle.model.Weather.Forecast;
import com.drizzle.android.drizzle.R;

/**
 * Created by macbookpro on 02.11.15.
 */
public class TabStandard extends Fragment {

    private Double mTemperature;
    private Double mTemperatureMin;
    private Double mTemperatureMax;
    private Double mPrecipProbNow;
    private Double mPrecipProbToday;

    private String mAddress;
    private String mSummaryString;
    private String mSunriseString;
    private String mSunsetString;
    private String mIcon;

    private ImageView mIconWeather;
    private TextView mTempLabel;
    private TextView mTempLabelMin;
    private TextView mTempLabelMax;
    private TextView mAddressLabel;
    private TextView mPrecipProbNowLabel;
    private TextView mPrecipProbTodayLabel;
    private TextView mSummary;
    private TextView mSunrise;
    private TextView mSunset;

    private static final String sAuthor = "author: Filip Lubniewski";
    private static final String sImagesSource = "images: icons8.com";
    private static final String sLogoAuthor = "logo: Mateusz Kurpias";
    private static final String sCelsius = "\u2103";

    public static TabStandard newInstance(Forecast forecast) {

        TabStandard tabStandard = new TabStandard();
        Currently data = forecast.getCurrently();

        Bundle args = new Bundle();
        args.putString("ICON", data.getIcon());
        args.putDouble("TEMP", data.getApparentTemperature());
        args.putDouble("MIN_TEMP", data.getApparentTemperatureMin());
        args.putDouble("MAX_TEMP", data.getApparentTemperatureMax());
        args.putString("ADDRESS", forecast.getAddress());
        args.putDouble("PRECIP_PROB_NOW", data.getPrecipProbNow());
        args.putDouble("PRECIP_PROB_TODAY", data.getPrecipProbToday());
        args.putString("SUMMARY", data.getSummaryToday());
        args.putString("SUNRISE", data.getFormattedTime(data.getSunrise()));
        args.putString("SUNSET", data.getFormattedTime(data.getSunset()));

        tabStandard.setArguments(args);
        return tabStandard;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        mIcon = args.getString("ICON");
        mTemperature = args.getDouble("TEMP");
        mTemperatureMin = args.getDouble("MIN_TEMP");
        mTemperatureMax = args.getDouble("MAX_TEMP");
        mAddress = args.getString("ADDRESS");
        mPrecipProbNow = args.getDouble("PRECIP_PROB_NOW");
        mPrecipProbToday = args.getDouble("PRECIP_PROB_TODAY");
        mSummaryString = args.getString("SUMMARY");
        mSunriseString = args.getString("SUNRISE");
        mSunsetString = args.getString("SUNSET");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.standard, container, false);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle("About")
                        .setMessage(sAuthor + "\n" + sImagesSource + "\n" + sLogoAuthor)
                        .show();
            }
        };

        TextView aboutButton = (TextView) v.findViewById(R.id.about);
        aboutButton.setOnClickListener(listener);

        setViews(v);

        return v;
    }

    private void setViews(View v) {
        mTempLabel = (TextView) v.findViewById(R.id.temperatureValue);
        mTempLabelMin = (TextView) v.findViewById(R.id.minAppTempVal);
        mTempLabelMax = (TextView) v.findViewById(R.id.maxAppTempVal);
        mAddressLabel = (TextView) v.findViewById(R.id.locationLabel);
        mPrecipProbNowLabel = (TextView) v.findViewById(R.id.precipValNow);
        mPrecipProbTodayLabel = (TextView) v.findViewById(R.id.precipValToday);
        mSummary = (TextView) v.findViewById(R.id.dailySummaryText);
        mSunrise = (TextView) v.findViewById(R.id.sunriseTime);
        mSunset = (TextView) v.findViewById(R.id.sunsetTime);
        mIconWeather = (ImageView) v.findViewById(R.id.weatherIcon);

        mIconWeather = (ImageView) v.findViewById(R.id.weatherIcon);
        mTempLabel.setText(Math.round(mTemperature) + "\u2103");
        mTempLabelMin.setText("min: " + Math.round(mTemperatureMin) + sCelsius);
        mTempLabelMax.setText("max: " + Math.round(mTemperatureMax) + sCelsius);
        mAddressLabel.setText(mAddress);
        mPrecipProbNowLabel.setText("now: " + Math.round(mPrecipProbNow * 100) + "%");
        mPrecipProbTodayLabel.setText("today: " + Math.round(mPrecipProbToday * 100) + "%");
        mSummary.setText(mSummaryString);
        mSunrise.setText("sunrise: " + mSunriseString);
        mSunset.setText("sunset: " + mSunsetString);

        //clear-day, clear-night, rain, snow, sleet, wind, fog, cloudy, partly-cloudy-day, or partly-cloudy-night

        if(mIcon.equals("clear-day")) {
            setIcon(R.drawable.sun);
        } else if(mIcon.equals("clear-night")) {
            setIcon(R.drawable.moon);
        } else if(mIcon.equals("rain")) {
            setIcon(R.drawable.rain);
        } else if(mIcon.equals("snow")) {
            setIcon(R.drawable.snow);
        } else if(mIcon.equals("sleet")) {
            setIcon(R.drawable.sleet);
        } else if(mIcon.equals("wind")) {
            setIcon(R.drawable.wind);
        } else if(mIcon.equals("fog")) {
            setIcon(R.drawable.fog);
        } else if(mIcon.equals("cloudy")) {
            setIcon(R.drawable.clouds);
        } else if(mIcon.equals("partly-cloudy-day")) {
            setIcon(R.drawable.partly_cloud_day);
        } else if(mIcon.equals("partly-cloudy-night")) {
            setIcon(R.drawable.partly_cloud_night);
        } else  { setIcon(R.drawable.sun); }
    }

    private void setIcon(int id) {
        mIconWeather.setImageResource(id);
    }
}
