package com.drizzle.android.drizzle.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.drizzle.android.drizzle.model.Geolocation;
import com.drizzle.android.drizzle.model.Weather.Forecast;
import com.drizzle.android.drizzle.R;
import com.drizzle.android.drizzle.view.SlidingTabFragments.ViewPagerCustomAdapter;
import com.drizzle.android.drizzle.view.SlidingTabsGoogle.SlidingTabLayout;

import org.json.JSONException;
import org.json.JSONObject;

//Author: Filip Lubniewski

public class MainActivity extends AppCompatActivity {

    //Variables for objects with position fixing, progress shown during position fixing
    // and saving retrieved json data
    private Geolocation mGeolocation;
    private ProgressDialog mDialogProgressCoordinates;
    private ProgressDialog mDialogProgressDataFromInternet;
    private Forecast mForecast;

    //Essential data for api connection
    private static final String sAPIKey = "d9ab2f20dc0112f29c3fdf8c6730d634";
    private static final String sURL = "https://api.forecast.io/forecast/";
    private static final String sUnits ="units=si";

    //Elements to display
    private ViewPager mViewPager;
    private SlidingTabLayout mSlidingTabs;
    private ViewPagerCustomAdapter mAdapter;

    //Titles of pages in ViewPager
    private static final CharSequence[] sTitles = {"Standard", "Sport & Health"};
    //Number of pages shown in ViewPager
    private static final int numberOfPages = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Getting id of elements to display
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mSlidingTabs = (SlidingTabLayout) findViewById(R.id.slidingTabs);

        //Setting listener to load layout if coordinates were found
        Geolocation.OnLocationGet listener = new Geolocation.OnLocationGet() {
            @Override
            public void onLocationGet(double longitude, double latitude, final String address) {

                //Hide progress dialog informing about fixing position
                mDialogProgressCoordinates.hide();

                //TODO: sprawdzic polaczenie z internetem

                //Our forecast.io API request URL with units in Celsius.
                String requestURL =
                        sURL + sAPIKey + "/" + latitude + "," + longitude + "?" + sUnits;

                //New request thanks to volley library
                JsonObjectRequest jsonRequest = new JsonObjectRequest
                        (Request.Method.GET, requestURL, (String) null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    //Hide dialog with information about progress of getting the data
                                    mDialogProgressDataFromInternet.hide();
                                    //Setting retrieved data
                                    mForecast = Forecast.setForecast(response);
                                    //Setting address based on longitude and latitude
                                    mForecast.setAddress(address);
                                    //Setting adapter and layout sliding tab
                                    setViewPagerAndSlidingTabs(mForecast);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });

                //Start the dialog showing progress of getting the data from api
                setDialogProgressDataFromInternet();

                //Adding request to queue
                Volley.newRequestQueue(getApplicationContext()).add(jsonRequest);
            }
        };

        //New object for position fixing
        mGeolocation = new Geolocation(getApplicationContext(), listener);

        //Dialog which is shown during position fixing
        setDialogProgressCoordinates();

        //Start fixing position
        mGeolocation.startLocationSearch();
    }


    private void setViewPagerAndSlidingTabs(Forecast mForecast) {
        //New custom adapter with passing data from api to inflate layouts with proper info
        mAdapter = new ViewPagerCustomAdapter(getSupportFragmentManager(), mForecast, sTitles, numberOfPages);
        //Setting custom adapter on ViewPager in layout with inflated fragments
        mViewPager.setAdapter(mAdapter);
        //Evenly distributed tabs
        mSlidingTabs.setDistributeEvenly(true);
        //White text color in tab while selected
        mSlidingTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.DividerColor);
            }
        });
        mSlidingTabs.setViewPager(mViewPager);
    }

    //Dialogs..
    private void setDialogProgressCoordinates() {
        mDialogProgressCoordinates = new ProgressDialog(this);
        mDialogProgressCoordinates.setMessage
                ("We're are trying to determine\nwhere you are. Please wait.\nThank you.");
        mDialogProgressCoordinates.show();
    }

    private void setDialogProgressDataFromInternet() {
        mDialogProgressDataFromInternet = new ProgressDialog(this);
        mDialogProgressDataFromInternet.setMessage("Loading data from internet.\n" +
                "Please wait a few moments.");
        mDialogProgressDataFromInternet.show();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
