package com.drizzle.android.drizzle.model.Weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by macbookpro on 31.10.15.
 */
public class Currently {
    private long mTime;
    private double mApparentTemperature;
    private double mApparentTemperatureMin;
    private double mApparentTemperatureMax;
    private double mPrecipProbNow;
    private double mPrecipProbToday;
    private String mSummaryWeek;
    private String mSummaryToday;
    private String mSummaryNow;
    private String mTimezone;
    private String mIcon;
    private long mSunset;
    private long mSunrise;

    protected static Currently setCurrentlyData(JSONObject data) throws JSONException {
        Currently currently = new Currently();
        JSONObject jsonCurrently = data.getJSONObject("currently");

        //Data for now
        currently.setTimezone(data.getString("timezone"));
        currently.setTime(jsonCurrently.getLong("time"));
        currently.setApparentTemperature(jsonCurrently.getDouble("apparentTemperature"));
        currently.setIcon(jsonCurrently.getString("icon"));
        currently.setSummaryNow(jsonCurrently.getString("summary"));
        currently.setPrecipProbNow(jsonCurrently.getDouble("precipProbability"));


        //Data for today
        JSONObject jsonDaily = data.getJSONObject("daily");
        currently.setSummaryWeek(jsonDaily.getString("summary"));

        JSONArray jsonArrayDaily = jsonDaily.getJSONArray("data");
        JSONObject firstArrayObject = jsonArrayDaily.getJSONObject(0);

        currently.setSummaryToday(firstArrayObject.getString("summary"));
        currently.setPrecipProbToday(firstArrayObject.getDouble("precipProbability"));
        currently.setApparentTemperatureMin(firstArrayObject.getDouble("apparentTemperatureMin"));
        currently.setApparentTemperatureMax(firstArrayObject.getDouble("apparentTemperatureMax"));
        currently.setSunrise(firstArrayObject.getLong("sunriseTime"));
        currently.setSunset(firstArrayObject.getLong("sunsetTime"));

        return currently;
    }

    public long getSunset() {
        return mSunset;
    }

    public void setSunset(long sunset) {
        mSunset = sunset;
    }

    public long getSunrise() {
        return mSunrise;
    }

    public void setSunrise(long sunrise) {
        mSunrise = sunrise;
    }

    public double getApparentTemperatureMin() {
        return mApparentTemperatureMin;
    }

    public void setApparentTemperatureMin(double apparentTemperatureMin) {
        mApparentTemperatureMin = apparentTemperatureMin;
    }

    public double getApparentTemperatureMax() {
        return mApparentTemperatureMax;
    }

    public void setApparentTemperatureMax(double apparentTemperatureMax) {
        mApparentTemperatureMax = apparentTemperatureMax;
    }

    public double getPrecipProbNow() {
        return mPrecipProbNow;
    }

    public void setPrecipProbNow(double precipProbNow) {
        mPrecipProbNow = precipProbNow;
    }

    public double getPrecipProbToday() {
        return mPrecipProbToday;
    }

    public void setPrecipProbToday(double precipProbToday) {
        mPrecipProbToday = precipProbToday;
    }

    public String getSummaryNow() {
        return mSummaryNow;
    }

    public void setSummaryNow(String summaryNow) {
        mSummaryNow = summaryNow;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    public double getApparentTemperature() {
        return mApparentTemperature;
    }

    public void setApparentTemperature(double apparentTemperature) {
        mApparentTemperature = apparentTemperature;
    }

    public String getSummaryWeek() {
        return mSummaryWeek;
    }

    public void setSummaryWeek(String summaryWeek) {
        mSummaryWeek = summaryWeek;
    }

    public String getSummaryToday() {
        return mSummaryToday;
    }

    public void setSummaryToday(String summaryToday) {
        mSummaryToday = summaryToday;
    }

    public String getTimezone() {
        return mTimezone;
    }

    public void setTimezone(String timezone) {
        mTimezone = timezone;
    }

    public String getIcon() {
        return mIcon;
    }

    public void setIcon(String icon) {
        mIcon = icon;
    }

    public String getFormattedTime(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("kk:mm");
        formatter.setTimeZone(TimeZone.getTimeZone(getTimezone()));
        Date timeDate = new Date(time * 1000);
        String timeString = formatter.format(timeDate);
        return timeString;
    }
}
