package com.drizzle.android.drizzle.model;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class Geolocation {

    LocationManager mLocationManager;

    Context mContext;
    private double mLongitude;
    private double mLatitude;
    private String mAddress;

    private OnLocationGet mListener;

    public interface OnLocationGet {
        void onLocationGet(double longitude, double latitude, String address);
    }

    public Geolocation(Context context, OnLocationGet listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    public void startLocationSearch() {
        mLocationManager = (LocationManager) mContext
                .getSystemService(Context.LOCATION_SERVICE);

        try {

            final LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) throws SecurityException {

                    mLocationManager.removeUpdates(this);
                    mLongitude = location.getLongitude();
                    mLatitude = location.getLatitude();

                    mAddress = getYourAddress(mLatitude, mLongitude);

                    mListener.onLocationGet(mLongitude, mLatitude, mAddress);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        } catch (SecurityException e) {
            Log.e("PERMISSION_EXCEPTION", "PERMISSION_NOT_GRANTED");
        }
    }

    private String getYourAddress(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
        List<Address> yourAddress;
        String addressToReturn = "";

        try {
            yourAddress = geocoder.getFromLocation(latitude, longitude, 1);
            if(yourAddress.size() > 0) {
                addressToReturn = yourAddress.get(0).getAddressLine(1) + ", " +
                        yourAddress.get(0).getAddressLine(2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return addressToReturn;
    }
}
