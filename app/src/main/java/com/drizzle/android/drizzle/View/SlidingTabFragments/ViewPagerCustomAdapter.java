package com.drizzle.android.drizzle.view.SlidingTabFragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.drizzle.android.drizzle.model.Weather.Forecast;

/**
 * Created by macbookpro on 02.11.15.
 */
public class ViewPagerCustomAdapter extends FragmentStatePagerAdapter {
    private final CharSequence[] mTitles;
    private int mNumberOfTabs;
    private Forecast mForecast;

    public ViewPagerCustomAdapter(FragmentManager fragmentManager, Forecast forecast, CharSequence[] titles, int numberOfTabs) {
        super(fragmentManager);
        this.mNumberOfTabs = numberOfTabs;
        this.mTitles = titles;
        this.mForecast = forecast;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0)
            return TabStandard.newInstance(mForecast);
        else
            return TabSportAndHealth.newInstance(mForecast);
    }

    @Override
    public int getCount() {
        return mNumberOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}
